<html>
    <head>
        <meta h ttp-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="Style.css"/>
        <title>JSP Page</title>
    </head>

    <body>
        <header class="container"id="header"> 
            <table widht="100%">
                <td width="50%">
                    <h1>Sistem Informasi Akuntansi</h1>
                    <h2>Bina Sarana Informatika</h2>
                    <h3>Jl. Ir. Haji Juanda No.17 Sarimulya, Kotabaru, Kabupaten Karawang, Jawa Barat 41374, Indonesia</h3>
                </td>
                <td width"50%">
                    <img src="logo_bsi hijauy.png">
                </td>
            </table>
        </header>

        <!-- membuat menubar -->
        <nav class="container" id="nav">
            <ul>
                <li><a href="home.jsp">Home<a/></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Help</a></li>
            </ul>
        </nav>

        <!-- sidebar -->
        <section class="container" id="main">
            <aside id="sidebar">
                <u>Master Data</u>

                <ul>
                    <li><a href="tampil_akun.jsp">Akun</a></li>
                    <li><a href="saldo_awal.jsp">Saldo Awal</a></li>
                    <li><a href="#">User</a></li>
                    <li><a href="#">Periode Akuntansi</a></li>
                </ul>

                <u>Transaksi</u>
                <ul>
                    <li><a href="home.jsp">Kas Masuk</a></li>
                    <li><a href="#">Kas Kelaur</a></li>
                    <li><a href="#">Jurnal Umum</a></li>
                    <li><a href="#">Jurnal Penjualan</a></li>
                </ul>

                <u>Informasi Data</u>
                <ul>
                    <li><a href="home.jsp">Master Akun</a></li>
                    <li><a href="#">Saldo Awal</a></li>
                    <li><a href="#">Data Kas Masuk</a></li>
                    <li><a href="#">Kas Masuk</a></li>
                    <li><a href="#">User</a></li>
                </ul>
            </aside> <!-- /sidebar -->

            <!-- content -->
            <section id="content">
                <article>
                    <header>
      <h1>Tambah Akun</h1> 
        <form action="akunServlet" method="POST"> 
            <table border="0" cellpadding="4">
                <tbody>
                    <tr>
                        <td>Jenis Akun</td>
                        <td>
                            <select name="jenis">
                                <!--Jenis Akun disesuaikan sendiri-->
                                <option value="Kas/Bank" selected>Kas/Bank</option>
                                <option value="Aktiva Lancar">Aktiva Lancar</option>
                                <option value="Aktiva Tetap">Aktiva Tetap</option>
                                <option value="Kewajiban">Kewajiban</option>
                                <option value="Modal">Modal</option>
                                <option value="Pendapatan">Pendapatan</option>                               
                                <option value="Beban">Beban</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Kode*</td>
                        <td><input type="text" name="kode" size="5" /></td></tr>
                    <tr>
                        <td>Nama Akun</td>
                        <td><input type="text" name="nama" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Saldo Normal</td>
                        <td>
                            <select name="saldo">
                                <option value="Debet">Debet</option>
                                <option value="Kredit">Kredit</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" value="Insert" name="aksi" />
                            <input type="reset" value="Reset" name="batal" />                       
                        </td>
                    </tr>
                </tbody>
            </table> 
        </form> 

                    </header>
                </article>
            </section>
        </section> <!-- /main -->

        <div id="footer">
            copyright &copy; 2017
        </div>
    </body>
</html>
