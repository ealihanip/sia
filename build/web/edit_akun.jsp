<%@page import="java.sql.*, model.Akun" %>
<%
    Akun akun = new Akun();
    //--koneksi database--
    Connection koneksi = null;
    Statement stmt = null;
    ResultSet rs = null;
    Class.forName("com.mysql.jdbc.Driver");
    koneksi = DriverManager
            .getConnection("jdbc:mysql://localhost:3306/sia",
                    "root", "");
    stmt = koneksi.createStatement();
    String kode = request.getParameter("kode");
    if (kode != null) {
        rs = stmt.executeQuery("SELECT * FROM master_akun"
                + " WHERE kode_akun = '" + kode + "'");
        if (rs.next()) {
            akun.setKode_akun(rs.getString("kode_akun"));
            akun.setNama_akun(rs.getString("nama_akun"));
            akun.setJenis_akun(rs.getString("jenis_akun"));
            akun.setSaldo_normal(rs.getString("saldo_normal"));
        }
    }


%>

<html>
    <head>
        <meta h ttp-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="Style.css"/>
        <title>JSP Page</title>
    </head>

    <body>
        <header class="container"id="header"> 
            <table widht="100%">
                <td width="50%">
                    <h1>Sistem Informasi Akuntansi</h1>
                    <h2>Bina Sarana Informatika</h2>
                    <h3>Jl. Ir. Haji Juanda No.17 Sarimulya, Kotabaru, Kabupaten Karawang, Jawa Barat 41374, Indonesia</h3>
                </td>
                <td width"50%">
                    <img src="logo_bsi hijauy.png">
                </td>
            </table>
        </header>

        <!-- membuat menubar -->
        <nav class="container" id="nav">
            <ul>
                <li><a href="home.jsp">Home<a/></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Help</a></li>
            </ul>
        </nav>

        <!-- sidebar -->
        <section class="container" id="main">
            <aside id="sidebar">
                <u>Master Data</u>

                <ul>
                    <li><a href="tampil_akun.jsp">Akun</a></li>
                    <li><a href="saldo_awal.jsp">Saldo Awal</a></li>
                    <li><a href="#">User</a></li>
                    <li><a href="#">Periode Akuntansi</a></li>
                </ul>

                <u>Transaksi</u>
                <ul>
                    <li><a href="home.jsp">Kas Masuk</a></li>
                    <li><a href="#">Kas Kelaur</a></li>
                    <li><a href="#">Jurnal Umum</a></li>
                    <li><a href="#">Jurnal Penjualan</a></li>
                </ul>

                <u>Informasi Data</u>
                <ul>
                    <li><a href="home.jsp">Master Akun</a></li>
                    <li><a href="#">Saldo Awal</a></li>
                    <li><a href="#">Data Kas Masuk</a></li>
                    <li><a href="#">Kas Masuk</a></li>
                    <li><a href="#">User</a></li>
                </ul>
            </aside> <!-- /sidebar -->

            <!-- content -->
            <section id="content">
                <article>
                    <header>


                        <center><h1>Ubah Data Akun</h1></center>
                        <form action="akunServlet" method="POST">
                            <table border="0" cellpadding="4">
                                <tbody>
                                    <tr>
                                        <td valign="top">Jenis Akun</td>
                                        <td><select name="jenis">
                                                <!--Jenis Akun disesuaikan sendiri-->
                                                <% if (akun.getJenis_akun()
                                                            .equalsIgnoreCase("Kas/Bank")) { %>
                                                <option value="Kas/Bank"  selected>Kas/Bank</option>
                                                <option value="Aktiva Lancar">Aktiva Lancar</option>
                                                <option value="Aktiva Tetap">Aktiva Tetap</option>
                                                <option value="Kewajiban">Kewajiban</option>
                                                <option value="Modal">Modal</option>
                                                <option value="Pendapatan">Pendapatan</option>                               
                                                <option value="Beban">Beban</option>
                                                <% }%>
                                                <% if (akun.getJenis_akun()
                                                            .equalsIgnoreCase("Aktiva Lancar")) { %>
                                                <option value="Kas/Bank"  >Kas/Bank</option>
                                                <option value="Aktiva Lancar" selected>Aktiva Lancar</option>
                                                <option value="Aktiva Tetap">Aktiva Tetap</option>
                                                <option value="Kewajiban">Kewajiban</option>
                                                <option value="Modal">Modal</option>
                                                <option value="Pendapatan">Pendapatan</option>                               
                                                <option value="Beban">Beban</option>
                                                <% }%>
                                                <% if (akun.getJenis_akun()
                                                            .equalsIgnoreCase("Aktiva Tetap")) { %>
                                                <option value="Kas/Bank"  >Kas/Bank</option>
                                                <option value="Aktiva Lancar" >Aktiva Lancar</option>
                                                <option value="Aktiva Tetap" selected>Aktiva Tetap</option>
                                                <option value="Kewajiban">Kewajiban</option>
                                                <option value="Modal">Modal</option>
                                                <option value="Pendapatan">Pendapatan</option>                               
                                                <option value="Beban">Beban</option>
                                                <% }%>
                                                <% if (akun.getJenis_akun()
                                                            .equalsIgnoreCase("Kewajiban")) { %>
                                                <option value="Kas/Bank"  >Kas/Bank</option>
                                                <option value="Aktiva Lancar" >Aktiva Lancar</option>
                                                <option value="Aktiva Tetap">Aktiva Tetap</option>
                                                <option value="Kewajiban" selected>Kewajiban</option>
                                                <option value="Modal">Modal</option>
                                                <option value="Pendapatan">Pendapatan</option>                               
                                                <option value="Beban">Beban</option>
                                                <% }%>
                                                <% if (akun.getJenis_akun()
                                                            .equalsIgnoreCase("Modal")) { %>
                                                <option value="Kas/Bank"  >Kas/Bank</option>
                                                <option value="Aktiva Lancar" >Aktiva Lancar</option>
                                                <option value="Aktiva Tetap">Aktiva Tetap</option>
                                                <option value="Kewajiban" >Kewajiban</option>
                                                <option value="Modal" selected>Modal</option>
                                                <option value="Pendapatan">Pendapatan</option>                               
                                                <option value="Beban">Beban</option>
                                                <% }%>
                                                <% if (akun.getJenis_akun()
                                                            .equalsIgnoreCase("Pendapatan")) { %>
                                                <option value="Kas/Bank"  >Kas/Bank</option>
                                                <option value="Aktiva Lancar" >Aktiva Lancar</option>
                                                <option value="Aktiva Tetap">Aktiva Tetap</option>
                                                <option value="Kewajiban" >Kewajiban</option>
                                                <option value="Modal" >Modal</option>
                                                <option value="Pendapatan" selected>Pendapatan</option>                               
                                                <option value="Beban">Beban</option>
                                                <% }%>
                                                <% if (akun.getJenis_akun()
                                                            .equalsIgnoreCase("Beban")) { %>
                                                <option value="Kas/Bank"  >Kas/Bank</option>
                                                <option value="Aktiva Lancar" >Aktiva Lancar</option>
                                                <option value="Aktiva Tetap">Aktiva Tetap</option>
                                                <option value="Kewajiban" >Kewajiban</option>
                                                <option value="Modal" >Modal</option>
                                                <option value="Pendapatan" >Pendapatan</option>                               
                                                <option value="Beban" selected>Beban</option>
                                                <% }%>
                                            </select>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kode*</td>
                                        <td><input type="text" name="kode" size="5" 
                                                   value="<%= akun.getKode_akun()%>" 
                                                   readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Akun</td>
                                        <td><input type="text" name="nama" size="20" 
                                                   value="<%=akun.getNama_akun()%>" /></td>
                                    </tr>
                                    <tr>
                                        <td>Saldo Normal</td>
                                        <td>
                                            <select name="saldo">
                                                <%
                                                    if (akun.getSaldo_normal()
                                                            .equalsIgnoreCase("Debet")) { %>
                                                <option value="Debet" selected>Debet</option>
                                                <option value="Kredit">Kredit</option>
                                                <% } else { %>
                                                <option value="Debet">Debet</option>
                                                <option value="Kredit" selected>Kredit</option>
                                                <% }%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <input type="submit" value="Update" name="aksi" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>

                    </header>
                </article>
            </section>
        </section> <!-- /main -->

        <div id="footer">
            copyright &copy; 2017
        </div>
    </body>
</html>
