<%@page import="java.sql.*" %>

<%
    //--koneksi database--
    Connection koneksi = null;
    Statement stmt = null;
    ResultSet rs = null;
    Class.forName("com.mysql.jdbc.Driver");
    koneksi = DriverManager
            .getConnection("jdbc:mysql://localhost:3306/sia",
                    "root", "");
    stmt = koneksi.createStatement();
    rs = stmt.executeQuery("SELECT * FROM master_akun");

%>


<html>
    <head>
        <meta h ttp-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="Style.css"/>
        <title>JSP Page</title>
    </head>

    <body>
        <header class="container"id="header"> 
            <table widht="100%">
                <td width="50%">
                    <h1>Sistem Informasi Akuntansi</h1>
                    <h2>Bina Sarana Informatika</h2>
                    <h3>Jl. Ir. Haji Juanda No.17 Sarimulya, Kotabaru, Kabupaten Karawang, Jawa Barat 41374, Indonesia</h3>
                </td>
                <td width"50%">
                    <img src="logo_bsi hijauy.png">
                </td>
            </table>
        </header>

        <!-- membuat menubar -->
        <nav class="container" id="nav">
            <ul>
                <li><a href="home.jsp">Home<a/></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Help</a></li>
            </ul>
        </nav>

        <!-- sidebar -->
        <section class="container" id="main">
            <aside id="sidebar">
                <u>Master Data</u>

                <ul>
                    <li><a href="tampil_akun.jsp">Akun</a></li>
                    <li><a href="saldo_awal.jsp">Saldo Awal</a></li>
                    <li><a href="#">User</a></li>
                    <li><a href="#">Periode Akuntansi</a></li>
                </ul>

                <u>Transaksi</u>
                <ul>
                    <li><a href="home.jsp">Kas Masuk</a></li>
                    <li><a href="#">Kas Kelaur</a></li>
                    <li><a href="#">Jurnal Umum</a></li>
                    <li><a href="#">Jurnal Penjualan</a></li>
                </ul>

                <u>Informasi Data</u>
                <ul>
                    <li><a href="home.jsp">Master Akun</a></li>
                    <li><a href="#">Saldo Awal</a></li>
                    <li><a href="#">Data Kas Masuk</a></li>
                    <li><a href="#">Kas Masuk</a></li>
                    <li><a href="#">User</a></li>
                </ul>
            </aside> <!-- /sidebar -->

            <!-- content -->
            <section id="content" >
                <article>
                    <header>


                        <h1>Daftar Rekening [Akun]</h1>

                        <br>
                        <a href="add_akun.jsp">Tambah Akun</a>
                        <br>
                        <br>
                        <table border="1" class="tabel1">
                            <tr>
                                <th>Kode*</th>
                                <th>Nama Akun</th>
                                <th>Jenis Akun</th>
                                <th>Saldo Normal</th>
                                <th>Action</th>
                            </tr>
                            <%while (rs.next()) {

                                    out.println("<tr>"
                                            + "<td>" + rs.getString("kode_akun") + "</td>"
                                            + "<td>" + rs.getString("nama_akun") + "</td>"
                                            + "<td>" + rs.getString("jenis_akun") + "</td>"
                                            + "<td>" + rs.getString("saldo_normal") + "</td>"
                                            + "<td><a href=edit_akun.jsp?kode="
                                            + rs.getString("kode_akun") + ">Edit</a> | "
                                            + "<a href=akunServlet?aksi=Delete&kode="
                                            + rs.getString("kode_akun") + ">Hapus</a></td>"
                                            + "</tr>");
                                }

                            %>
                        </table>

                    </header>
                </article>
            </section>
        </section> <!-- /main -->

        <div id="footer">
            copyright &copy; 2017
        </div>
    </body>
</html>
